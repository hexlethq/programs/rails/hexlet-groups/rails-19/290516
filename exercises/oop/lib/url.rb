# frozen_string_literal: true

require 'forwardable'
require 'uri'

# BEGIN
class Url
  extend Forwardable
  include Comparable

  attr_reader :url

  def_delegators :@url, :scheme, :host

  def <=>(other)
    url <=> other.url
  end

  def initialize(url)
    @url = URI(url)
  end

  def query_params
    @query_params ||= url.query.split('&').map { |el| el.split('=') }.to_h.transform_keys(&:to_sym)
  end

  def query_param(key, default = nil)
    query_params.fetch(key, default)
  end
end
# END
