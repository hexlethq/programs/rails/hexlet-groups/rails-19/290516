# frozen_string_literal: true

# BEGIN
def build_query_string(params)
  params.sort_by { |key, _val| key }.map { |pair| "#{pair.first}=#{pair.last}" }.join('&')
end
# END
