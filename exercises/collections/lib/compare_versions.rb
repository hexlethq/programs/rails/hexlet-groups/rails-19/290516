# frozen_string_literal: true

# BEGIN
def compare_versions(version1, version2)
  arr1 = version1.split('.').map(&:to_i)
  arr2 = version2.split('.').map(&:to_i)

  arr1.each_with_index do |value1, index|
    next if (value1 == arr2[index]) && ((index + 1) != arr1.size)

    return value1 <=> arr2[index]
  end
end
# END
