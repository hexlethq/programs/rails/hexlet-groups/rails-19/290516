# frozen_string_literal: true

# BEGIN
def make_censored(text, stop_words)
  text.gsub(/[^,.\s]+/) { |word| stop_words.include?(word) ? '$#%!' : word }
end
# END
