# frozen_string_literal: true

require 'test_helper'

class CommentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @comment = comments(:one)
    @post = posts(:one)
  end

  test '#create' do
    assert_difference('Comment.count') do
      post post_comments_path, params: { comment: {
        body: @comment.body, post_id: @post.id } }
    end

    # asserts_redirected_to post_path(@post)
  end


  # test 'should create post' do
  #   assert_difference('Post.count') do
  #     post posts_url, params: { post: {
  #       title: 'Title',
  #       body: 'Body'
  #     } }
  #   end

  # test 'should get new' do
  #   get new_post_url
  #   assert_response :success
  # end

  # test 'should create post' do
  #   assert_difference('Post.count') do
  #     post posts_url, params: { post: {
  #       title: 'Title',
  #       body: 'Body'
  #     } }
  #   end

  #   assert_redirected_to post_url(Post.last)
  # end

  # test 'should show post' do
  #   get post_url(@post)
  #   assert_response :success
  # end

  # test 'should get edit' do
  #   get edit_post_url(@post)
  #   assert_response :success
  # end

  # test 'should update post' do
  #   patch post_url(@post), params: { post: {
  #     title: 'Title',
  #     body: 'Body'
  #   } }
  #   assert_redirected_to post_url(@post)
  # end

  # test 'should destroy post' do
  #   assert_difference('Post.count', -1) do
  #     delete post_url(@post)
  #   end

  #   assert_redirected_to posts_url
  # end
end
