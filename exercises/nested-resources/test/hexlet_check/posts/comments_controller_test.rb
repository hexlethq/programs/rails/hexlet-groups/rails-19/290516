# frozen_string_literal: true

require 'test_helper'

module HexletCheck
  module Posts
    class CommentsControllerTest < ActionDispatch::IntegrationTest
      setup do
        @comment = comments(:one)
        @post = @comment.post
      end

      test 'should create comment' do
        byebug
        assert_difference('Comment.count') do
          post post_comments_url(@post), params: { comment: {
            body: 'test'
          } }
        end

        assert_redirected_to post_url(@post)
      end

      test 'should destroy comment' do
        assert_difference('Comment.count', -1) do
          delete comment_url(@comment)
        end

        assert_redirected_to post_url(@post)
      end
    end
  end
end
