# frozen_string_literal: true

class Web::HomesController < ApplicationController
  def index; end
end
