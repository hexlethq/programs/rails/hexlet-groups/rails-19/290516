module Web
  class CommentsController < ApplicationController
    before_action :set_comment, only: [:edit, :update, :destroy]

    # def new
    # end

    def create
      @post = Post.find(params[:post_id])
      @comment = @post.comments.build(comment_params)
      if @comment.save
        redirect_to @post, notice: 'Comment was successfully created.'
      else
        render 'web/posts/show'
      end
    end

    def edit;end

    def update
      if @comment.update(comment_params)
        redirect_to @comment.post, notice: 'Comment was successfully updated.'
      else
        render :edit, comment: :unprocessable_entity
      end
    end

    def destroy
      post = @comment.post
      @comment.destroy
  
      redirect_to post, notice: 'Comment was successfully destroyed.'
    end
  
    private

    def comment_params
      params.require(:comment).permit(:body)
    end

    def set_comment
      @comment = Comment.find(params[:id])
    end
  end
end