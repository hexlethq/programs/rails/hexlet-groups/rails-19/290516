


5.times do |i|
  Post.create(title: "title#{i+1}", body: "body#{i+1}")
end

Post.all.each do |post|
  5.times do |i|
    comment = Comment.new(body: "body#{i+1}")
    post.comments << comment
    post.save
  end
end