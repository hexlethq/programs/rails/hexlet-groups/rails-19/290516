# frozen_string_literal: true

Rails.application.routes.draw do
  root 'web/homes#index'

  # BEGIN
  scope module: :web do
    resources :posts, shallow: :true do
      resources :comments
    end
  end
  # END

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
