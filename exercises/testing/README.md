# Тестирование в Ruby

Стек (англ. stack — стопка; читается стэк) — абстрактный тип данных, представляющий собой список элементов, организованных по принципу LIFO (англ. last in — first out, «последним пришёл — первым вышел»).

```ruby
stack = Stack.new
stack.to_a # []
stack.empty? # true
stack.size # 0

stack.push! 'ruby'
stack.push! 'php'
stack.push! 'java'
stack.to_a # ['ruby', 'php', 'java']
stack.size # 3
stack.empty? # false

stack.pop!
stack.to_a # ['ruby', 'php']
stack.size # 2

stack.clear!
stack.to_a # []
stack.empty? # true
```

## Ссылки

* [Minitest](http://docs.seattlerb.org/minitest/) - документация фреймворка тестирования

## Задачи

### test/stack_test.rb

Напишите тесты для реализации стека.

## Подсказки

* [Stack](https://ru.wikipedia.org/wiki/%D0%A1%D1%82%D0%B5%D0%BA) - что такое Stack
