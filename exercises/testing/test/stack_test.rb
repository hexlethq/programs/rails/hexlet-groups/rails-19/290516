# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def setup
    @stack = Stack.new([1, 2, 3, 4, 5])
  end

  def test_clear_method
    refute_empty @stack
    assert_empty @stack, @stack.clear!
  end

  def test_pop_method
    assert_equal 5, @stack.pop!
    assert_equal [1, 2, 3, 4], @stack.to_a
  end

  def test_push_method
    assert_equal [1, 2, 3, 4, 5, 6], @stack.push!(6)
  end

  def test_empty_method
    empty_stack = Stack.new
    assert_empty empty_stack, @stack.empty?
  end

  def test_to_a_method
    assert_equal [1, 2, 3, 4, 5], @stack.to_a
  end

  def test_size_method
    assert_equal 5, @stack.size
  end
  # END
end

test_methods = StackTest.new({}).methods.map(&:to_s).select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
