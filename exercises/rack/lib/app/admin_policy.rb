# frozen_string_literal: true

class AdminPolicy
  def initialize(app)
    @app = app
  end

  def call(env)
    req = Rack::Request.new(env)
    if req.path.match?(%r{^/admin})
      [403, { 'Content-Type' => 'text/plain' }, []]
    else
      @app.call(env)
    end
  end
end
