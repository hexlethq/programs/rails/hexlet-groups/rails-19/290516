# frozen_string_literal: true

require 'digest'

class Signature
  def initialize(app)
    @app = app
  end

  def call(env)
    status, headers, body = @app.call(env)
    [status, headers, add_digest_body(body)]
  end

  private

  def add_digest_body(body)
    body[0] += "\n#{Digest::SHA256.hexdigest body[0]}"
    body
  end
end
