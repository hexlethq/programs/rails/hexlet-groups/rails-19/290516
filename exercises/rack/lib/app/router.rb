# frozen_string_literal: true

class Router
  NOT_FOUND = [404, {}, ['404 Not Found']].freeze

  def call(env)
    req = Rack::Request.new(env)
    routes_mapper[req.path] || NOT_FOUND
  end

  private

  def routes_mapper
    {
      '/' => [200, { 'Content-Type' => 'text/plain' }, ['Hello, World!']],
      '/about' => [200, { 'Content-Type' => 'text/plain' }, ['About page']],
      '/admin' => [200, { 'Content-Type' => 'text/plain' }, ['Admin panel']]
    }
  end
end
