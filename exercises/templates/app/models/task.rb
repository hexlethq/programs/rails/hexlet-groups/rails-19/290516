class Task < ApplicationRecord
  validates :name, :status, :creator, presence: true
  validates_inclusion_of :completed, in: [true, false]

  def completed_human_value
    completed ? 'yes' : 'no'
  end
end
