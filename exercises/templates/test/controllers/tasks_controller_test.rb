require 'test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest
  test '#index' do
    get tasks_path

    assert_response :success
  end

  test '#new' do
    get new_task_path

    assert_response :success
  end

  test '#show' do
    get task_path(tasks(:one))

    assert_response :success
  end

  test '#create' do
    assert_difference('Task.count') do
      post tasks_path, params: { task: { name: 'name',
                                         description: 'description',
                                         status: 'status',
                                         creator: 'creator',
                                         performer: 'performer',
                                         completed: false } }
    end

    assert_redirected_to task_path(Task.last)
  end

  test '#edit' do
    get edit_task_path(tasks(:one))

    assert_response :success
  end

  test '#update' do
    task = tasks(:one)
    patch task_path(task), params: { task: { name: 'name',
                                             description: 'description',
                                             status: 'status',
                                             creator: 'creator',
                                             performer: 'performer',
                                             completed: false } }

    assert_redirected_to task_path(task)
  end

  test '#destroy' do
    task = tasks(:one)
    assert_difference('Task.count', -1) do
      delete task_path(task)
    end

    assert_redirected_to tasks_path
  end
end

