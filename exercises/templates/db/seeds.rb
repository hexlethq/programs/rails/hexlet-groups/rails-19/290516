5.times do |index|
  index += 1
  Task.create(
    name: "name_#{index}",
    description: "description_#{index}",
    status: ['critical', 'first-rate', 'normal', 'just relax, dude'].sample,
    creator: "iv",
    performer: "iv",
    completed: [true, false].sample
  )
end
