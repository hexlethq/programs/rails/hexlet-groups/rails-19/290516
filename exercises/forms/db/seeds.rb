5.times do |index|
  index += 1
  Post.create(
    title: "title_#{index}",
    body: "body_#{index}",
    summary: "summary_#{index}",
    published: [true, false].sample
  )
end
