# frozen_string_literal: true

# BEGIN
def count_by_years(users)
  males = users.reject do |user|
    user[:gender] == 'female'
  end

  males.map { |u| u[:birthday][0..3] }.group_by(&:itself).transform_values(&:size)
end
# END
