# frozen_string_literal: true

# BEGIN
def anagramm_filter(word, array_of_words)
  base = word.chars.sort
  array_of_words.filter { |w| w.chars.sort == base }
end
# END
