# frozen_string_literal: true

# BEGIN
def get_same_parity(arr)
  return arr if arr.empty?

  filter_method = arr.first.even? ? :even? : :odd?
  # arr.filter { |el| el.send(filter_method) }
  arr.filter(&filter_method)
end

# END
