# frozen_string_literal: true

class BulletinsController < ApplicationController
  # BEGIN
  def index
    @bulletins = Bulletin.all
  end

  def show
    @bulletin = Bulletin.find(params[:id])
    redirect_to root_path, flash: { error: 'The bulletin is not published' } unless @bulletin.published
  end
  # END
end
