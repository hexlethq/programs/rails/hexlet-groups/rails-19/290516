require 'test_helper'

class BulletinsControllerTest < ActionDispatch::IntegrationTest
  test 'opens all bulletins page' do
    get bulletins_path

    assert_response :success
  end

  test 'opens single bulletin page' do
    bulletin = bulletins(:published)
    get bulletin_path(bulletin)

    assert_response :success
  end

  test 'cant get unpublished bulletin' do
    get(bulletin_path(bulletins(:unpublished)))

    assert_response :redirect
    assert_redirected_to root_path
  end
end
