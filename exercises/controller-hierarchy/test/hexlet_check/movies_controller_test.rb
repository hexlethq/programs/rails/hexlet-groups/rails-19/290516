# frozen_string_literal: true

require 'test_helper'

class Web::MoviesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @movie = movies :star_wars
  end

  test 'should get index' do
    get movies_url
    assert_response :success
  end

  test 'should get show' do
    get movie_url(@movie)
    assert_response :success
  end

  test 'should get new' do
    get new_movie_url
    assert_response :success
  end

  test 'should get edit' do
    get edit_movie_url(@movie)
    assert_response :success
  end

  test 'should create' do
    new_movie_title = 'New some movie'

    assert_difference 'Movie.count', 1 do
      post movies_url, params: {
        movie: {
          title: new_movie_title
        }
      }
    end
    assert_response :redirect
  end

  test 'should update' do
    new_movie_title = 'New some movie'

    patch movie_url(@movie), params: {
      movie: {
        title: new_movie_title
      }
    }
    assert_response :redirect

    @movie.reload

    assert_equal new_movie_title, @movie.title
  end

  test 'should destroy' do
    assert_difference 'Movie.count', -1 do
      delete movie_url(@movie)
    end
    assert_response :redirect
  end
end
