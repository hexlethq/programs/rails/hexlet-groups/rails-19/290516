# frozen_string_literal: true

require 'test_helper'

class Web::Movies::CommentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @movie = movies :lord_of_the_rings
    @comment = comments :one
  end

  test 'should get index' do
    get movie_comments_url(@movie)
    assert_response :success
  end

  test 'should get new' do
    get new_movie_comment_url(@movie)
    assert_response :success
  end

  test 'should get edit' do
    get edit_movie_comment_url(@movie, @comment)
    assert_response :success
  end

  test 'should create' do
    new_comment_body = 'New some comment'

    assert_difference 'Comment.count', 1 do
      post movie_comments_url(@movie), params: {
        comment: {
          body: new_comment_body
        }
      }
    end
    assert_response :redirect
  end

  test 'should update' do
    new_comment_body = 'New some comment'

    patch movie_comment_url(@movie, @comment), params: {
      comment: {
        body: new_comment_body
      }
    }
    assert_response :redirect

    @comment.reload

    assert_equal new_comment_body, @comment.body
  end

  test 'should destroy' do
    assert_difference 'Comment.count', -1 do
      delete movie_comment_url(@movie, @comment)
    end
    assert_response :redirect
  end
end
