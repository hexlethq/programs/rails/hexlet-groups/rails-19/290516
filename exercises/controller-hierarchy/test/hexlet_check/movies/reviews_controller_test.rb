# frozen_string_literal: true

require 'test_helper'

class Web::Movies::ReviewsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @movie = movies :lord_of_the_rings
    @review = reviews :one
  end

  test 'should get index' do
    get movie_reviews_url(@movie)
    assert_response :success
  end

  test 'should get new' do
    get new_movie_review_url(@movie)
    assert_response :success
  end

  test 'should get edit' do
    get edit_movie_review_url(@movie, @review)
    assert_response :success
  end

  test 'should create' do
    new_review_body = 'New some review'

    assert_difference 'Review.count', 1 do
      post movie_reviews_url(@movie), params: {
        review: {
          body: new_review_body
        }
      }
    end
    assert_response :redirect
  end

  test 'should update' do
    new_review_body = 'New some review'

    patch movie_review_url(@movie, @review), params: {
      review: {
        body: new_review_body
      }
    }
    assert_response :redirect

    @review.reload

    assert_equal new_review_body, @review.body
  end

  test 'should destroy' do
    assert_difference 'Review.count', -1 do
      delete movie_review_url(@movie, @review)
    end
    assert_response :redirect
  end
end
