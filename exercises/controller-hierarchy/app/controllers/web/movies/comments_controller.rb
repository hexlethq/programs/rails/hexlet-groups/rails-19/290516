# frozen_string_literal: true

module Web
  module Movies
    class CommentsController < Web::Movies::ApplicationController
      before_action :set_comment, only: %i[edit update destroy]

      def index
        @comments = resource_movie.comments.order(id: :desc)
      end

      def new
        @comment = resource_movie.comments.build
      end

      def create
        @comment = resource_movie.comments.build(permitted_comment_params)

        if @comment.save
          redirect_to movie_comments_path(resource_movie), notice: t('success')
        else
          render :new, notice: t('fail')
        end
      end

      def edit; end

      def update
        if @comment.update(permitted_comment_params)
          redirect_to movie_comments_path(resource_movie), notice: t('success')
        else
          render :edit, notice: t('fail')
        end
      end

      def destroy
        if @comment.destroy
          redirect_to movie_comments_path(resource_movie), notice: t('success')
        else
          redirect_to movie_comments_path(resource_movie), notice: t('fail')
        end
      end

      private

      def permitted_comment_params
        params.require(:comment).permit(:body)
      end

      def set_comment
        @comment = resource_movie.comments.find(params[:id])
      end
    end
  end
end
