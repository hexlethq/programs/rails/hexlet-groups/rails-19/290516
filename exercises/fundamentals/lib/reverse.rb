# frozen_string_literal: true

# BEGIN
def reverse(str)
  reversed = ''
  (0..str.length - 1).each do |i|
    reversed = str[i] + reversed
  end
  reversed
end
# END
