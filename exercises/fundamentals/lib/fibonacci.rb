# frozen_string_literal: true

# BEGIN
def fibonacci(index)
  arr = [0, 1]
  return nil if index.negative?

  arr << (arr[-1] + arr[-2]) while index > arr.size
  arr[index - 1]
end
# END
