# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  return '' if start > stop
  return start.to_s if start == stop

  result = ''
  (start..stop).each do |n|
    result += filtered_value(n)
    result += ' ' unless n == stop
  end
  result
end

def filtered_value(num)
  fizz = 'Fizz' if (num % 3).zero?
  buzz = 'Buzz' if (num % 5).zero?
  fizz || buzz ? fizz.to_s + buzz.to_s : num.to_s
end
# END
