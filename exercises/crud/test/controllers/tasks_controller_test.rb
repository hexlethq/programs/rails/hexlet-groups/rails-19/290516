require 'test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest
  test 'gets tasks index' do
    get tasks_path

    assert_response :success
  end

  test 'gets task new' do
    get new_task_path

    assert_response :success
  end

  test 'gets task show' do
    get task_path(tasks(:one))

    assert_response :success
  end

  test 'creates new task' do
    assert_difference('Task.count') do
      post tasks_path, params: { task: { name: 'name',
                                         description: 'description',
                                         status: 'status',
                                         creator: 'creator',
                                         performer: 'performer',
                                         completed: false } }
    end

    assert_redirected_to task_path(Task.last)
  end

  test 'gets task edit' do
    get edit_task_path(tasks(:one))

    assert_response :success
  end

  test 'updates task' do
    task = tasks(:one)
    patch task_path(task), params: { task: { name: 'name',
                                             description: 'description',
                                             status: 'status',
                                             creator: 'creator',
                                             performer: 'performer',
                                             completed: false } }

    assert_redirected_to task_path(task)
  end

  test 'deletes task' do
    task = tasks(:one)
    assert_difference('Task.count', -1) do
      delete task_path(task)
    end

    assert_redirected_to tasks_path
  end
end
