require 'test_helper'

class TaskFlowTest < ActionDispatch::IntegrationTest
  test 'opens tasks index page' do
    get tasks_path

    assert_select 'h1', 'All tasks'
  end

  test 'opens task new page' do
    get new_task_path

    assert_select 'h1', 'Add new task'
  end

  test 'opens task show page' do
    task = tasks(:one)
    get task_path(task)

    assert_select 'h1', "Task #{task.id}"
  end

  test 'opens task edit page' do
    task = tasks(:one)
    get edit_task_path(task)

    assert_select 'h1', 'Edit task'
  end

  test 'creates task' do
    get new_task_path
    assert_response :success

    post tasks_path, params: { task: { name: 'created task',
                                       description: 'description',
                                       status: 'status',
                                       creator: 'creator',
                                       performer: 'performer',
                                       completed: false } }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_select 'p', 'created task'
  end

  test 'updates task' do
    task = tasks(:one)
    get edit_task_path(task)
    assert_response :success

    patch task_path(task), params: { task: { name: 'updated task',
                                             description: 'description',
                                             status: 'status',
                                             creator: 'creator',
                                             performer: 'performer',
                                             completed: false } }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_select 'p', 'updated task'
  end
end
