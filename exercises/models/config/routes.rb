# frozen_string_literal: true

Rails.application.routes.draw do
  # BEGIN
  root 'articles#index'
  
  resources :articles, only: [:index, :show]
  # END
end
