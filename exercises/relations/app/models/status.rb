# frozen_string_literal: true

# == Schema Information
#
# Table name: statuses
#
#  id         :integer          not null, primary key
#  name       :string           default("New"), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  task_id    :integer          not null
#
# Indexes
#
#  index_statuses_on_task_id  (task_id)
#
# Foreign Keys
#
#  task_id  (task_id => tasks.id)
#
class Status < ApplicationRecord
  # BEGIN
  belongs_to :task

  NAMES = ['New', 'In progress', 'Done'].freeze
  validates :name, inclusion: { in: NAMES }
  # END
end
