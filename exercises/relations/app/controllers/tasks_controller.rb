# frozen_string_literal: true

class TasksController < ApplicationController
  before_action :set_task, only: %i[show edit update destroy]

  def index
    @tasks = Task.all
  end

  def show; end

  def new
    @task = Task.new
    @task.build_status
  end

  def edit; end

  def create
    @task = Task.new(task_params)
    @task.build_status(status_params)
    if @task.save
      redirect_to @task, notice: 'Task was successfully created.'
    else
      render :new, task: :unprocessable_entity
    end
  end

  def update
    if ActiveRecord::Base.transaction { @task.update(task_params) && @task.status.update(status_params) }
      redirect_to @task, notice: 'Task was successfully updated.'
    else
      render :edit, task: :unprocessable_entity
    end
  end

  def destroy
    @task.destroy

    redirect_to tasks_url, notice: 'Task was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_task
    @task = Task.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def task_params
    params.require(:task).permit(:name, :description, :user_id)
  end

  def status_params
    params.require(:task).require(:status_attributes).permit(:name)
  end
end
