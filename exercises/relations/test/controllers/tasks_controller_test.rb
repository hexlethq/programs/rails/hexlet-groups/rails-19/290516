require 'test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:one)
    @task = tasks(:one)
    @status = statuses(:one)
  end

  test '#index' do
    get tasks_path
    assert_response :success
  end

  test '#new' do
    get new_task_path
    assert_response :success
  end

  test '#create' do
    assert_difference('Task.count') do
      assert_difference('Status.count') do
        post tasks_path, params: { task: {
          name: @task.name,
          description: @task.description,
          user_id: @user.id,
          status_attributes: { name: @status.name }
        } }
      end
    end
  end

  test '#show' do
    get task_path(@task)
    assert_response :success
  end

  test '#edit' do
    get edit_task_path(@task)
    assert_response :success
  end

  test '#update' do
    patch task_path(@task), params: { task: { name: @task.name,
                                              status_attributes: { name: @status.name } } }
    assert_redirected_to task_path(@task)
  end

  test '#destroy' do
    assert_difference('Task.count', -1) do
      assert_difference('Status.count', -1) do
        delete task_path(@task)
      end
    end
    assert_redirected_to tasks_path
  end
end
