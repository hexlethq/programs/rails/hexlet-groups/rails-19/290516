class AddDefaultToStatusName < ActiveRecord::Migration[6.1]
  def change
    change_column :statuses, :name, :string, default: 'New'
  end
end
