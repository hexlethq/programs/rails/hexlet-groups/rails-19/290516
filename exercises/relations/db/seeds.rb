users = []
5.times do
  users << User.create(name: Faker::Name.name)
end


tasks = []
10.times do 
  tasks << Task.create(name: Faker::Nation.language,
                       description: Faker::Lorem.paragraph,
                       user: users.sample)
end


tasks.each do |task|
  Status.create(name: Status::NAMES.sample, task: task)
end