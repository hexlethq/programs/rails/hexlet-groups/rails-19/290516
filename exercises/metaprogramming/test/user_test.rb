# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/user'

class UserTest < Minitest::Test
  # BEGIN
  def test_user
    attributes = { name: 'Tony', birthday: '01/01/1960', active: false }
    result_attributes = { name: 'Tony', birthday: DateTime.parse('01.01.1960'), active: false }

    user = User.new(attributes)
    assert_equal result_attributes, user.attributes
  end

  def test_attribute_accessors # rubocop:disable Metrics/AbcSize
    attributes = { name: 'Tony', birthday: '01/01/1960', active: true }
    user = User.new(attributes)

    assert_equal attributes[:name], user.name
    assert_equal DateTime.parse(attributes[:birthday]), user.birthday
    assert_equal attributes[:active], user.active

    new_attributes = { name: 'Pussy', birthday: '02/02/1960', active: false }

    user.name = new_attributes[:name]
    user.birthday = new_attributes[:birthday]
    user.active = new_attributes[:active]

    assert_equal new_attributes[:name], user.name
    assert_equal DateTime.parse(new_attributes[:birthday]), user.birthday
    assert_equal new_attributes[:active], user.active

    user.name = nil
    assert_nil user.name
  end

  def test_defaults
    user = User.new(birthday: '02/02/1960')
    result_attributes = { name: 'Andrey', birthday: DateTime.parse('02/02/1960'), active: false }
    assert_equal result_attributes, user.attributes
  end

  def test_nil_value
    attributes = { name: nil, birthday: '01/01/1960', active: false }
    result_attributes = { name: nil, birthday: DateTime.parse('01.01.1960'), active: false }

    user = User.new(attributes)
    assert_equal result_attributes, user.attributes
  end

  def test_attribute_convert
    user = User.new(name: 1234, birthday: '02/02/1960', active: 'false')

    result_attributes = { name: '1234', birthday: DateTime.parse('02/02/1960'), active: false }

    assert_equal result_attributes, user.attributes
    assert_instance_of DateTime, user.birthday
    assert_instance_of FalseClass, user.active
  end
  # END
end
