# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/post'

class PostTest < Minitest::Test
  def test_post
    attributes = { title: 'Hello World', body: 'First post!', published: false }
    result_attributes = { id: nil, title: 'Hello World', body: 'First post!', created_at: nil, published: false }

    post = Post.new(attributes)
    assert_equal result_attributes, post.attributes
  end

  def test_attribute_accessors # rubocop:disable Metrics/AbcSize
    attributes = { id: 1, title: 'Hello World', body: 'First post!', created_at: '01/01/2021', published: false }
    post = Post.new attributes

    assert_equal attributes[:id], post.id
    assert_equal attributes[:title], post.title
    assert_equal attributes[:body], post.body
    assert_equal DateTime.parse(attributes[:created_at]), post.created_at
    assert_equal attributes[:published], post.published

    new_attributes = { id: nil, title: 'Edited title', body: 'First post! Updated', created_at: '01/03/2021',
                       published: true }
    post.id = new_attributes[:id]
    post.title = new_attributes[:title]
    post.body = new_attributes[:body]
    post.created_at = new_attributes[:created_at]
    post.published = new_attributes[:published]

    assert_nil post.id
    assert_equal new_attributes[:title], post.title
    assert_equal new_attributes[:body], post.body
    assert_equal DateTime.parse(new_attributes[:created_at]), post.created_at
    assert_equal new_attributes[:published], post.published

    post.title = nil
    assert_nil post.title
  end

  def test_nil_value
    attributes = { title: nil, body: 'First post!', published: false }
    result_attributes = { id: nil, title: nil, body: 'First post!', created_at: nil, published: false }

    post = Post.new(attributes)
    assert_equal result_attributes, post.attributes
  end

  def test_posts_not_equals
    post1 = Post.new title: 'test1'
    post2 = Post.new title: 'test2'

    assert_equal 'test1', post1.title
    assert_equal 'test2', post2.title
    refute_equal post1.title, post2.title
  end

  def test_attribute_convert
    post = Post.new id: '1', created_at: '01/01/2021', title: 123_123, published: 'yes'
    result_attributes = { id: 1, title: '123123', body: nil, created_at: DateTime.parse('01/01/2021'), published: true }
    assert_equal result_attributes, post.attributes
    assert_instance_of DateTime, post.created_at
  end
end
