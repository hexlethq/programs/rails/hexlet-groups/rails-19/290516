# frozen_string_literal: true

# BEGIN
module Model
  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods
    def schema
      @schema ||= {}
    end

    def attribute(attr_name, options = {})
      schema[attr_name] = options
      define_method("#{attr_name}=") do |value|
        attributes[attr_name] = convert_attr(attr_name, value)
      end

      define_method(attr_name) do
        attributes[attr_name]
      end
    end
  end

  def initialize(attrs = {})
    @result_attrs = {}
    build_attributes(attrs)
  end

  def attributes
    @result_attrs
  end

  private

  def build_attributes(attrs)
    schema = self.class.schema
    schema.each do |k, v|
      attributes[k] = attrs.key?(k) ? convert_attr(k, attrs[k]) : v[:default]
    end
  end

  def convert_attr(attr_name, value)
    return nil if value.nil?

    type = self.class.schema.dig(attr_name.to_sym, :type)
    mapping = convert_mapping
    raise StandardError, "attribute can`t be converted to: \"#{type}\"" unless mapping.keys.include?(type)

    mapping[type].call(value)
  end

  def convert_mapping
    to_boolean = lambda do |x|
      return true if ['yes', 'ok', 'true', true].include?(x)
      return false if ['no', 'not', 'false', false].include?(x)

      raise ArgumentError, "invalid value for Boolean: \"#{x}\""
    end

    { integer: (->(x) { x&.to_i }),
      string: (->(x) { x&.to_s }),
      datetime: (->(x) { DateTime.parse(x) }),
      boolean: to_boolean }
  end
end
# END
