# frozen_string_literal: true

require 'test_helper'

module HexletCheck
  class Web::UsersControllerTest < ActionDispatch::IntegrationTest
    setup do
      @user = users(:one)
    end

    test 'should get index' do
      get users_url
      assert_response :success
    end

    test 'can create user' do
      assert_difference('User.count') do
        post users_url, params: { user: {
          first_name: 'Name',
          email: 'email@test.io'
        } }
      end

      assert_redirected_to user_url(User.last)
    end

    test 'should show user' do
      get user_url(@user)
      assert_response :success
    end

    test 'can edit' do
      get edit_user_url(@user)
      assert_response :success
    end

    test 'can update user' do
      new_user_name = 'New User Name'

      patch user_url(@user), params: { user: {
        first_name: new_user_name
      } }
      assert_redirected_to user_url(@user)

      @user.reload

      assert_equal new_user_name, @user.first_name
    end

    test 'can destroy user' do
      assert_difference('User.count', -1) do
        delete user_url(@user)
      end

      assert_redirected_to users_url
    end
  end
end
