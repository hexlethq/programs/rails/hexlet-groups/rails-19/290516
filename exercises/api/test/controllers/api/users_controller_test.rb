# frozen_string_literal: true

require 'test_helper'

class Api::UsersControllerTest < ActionDispatch::IntegrationTest
  # BEGIN
  test '#index' do
    get api_users_path, as: :json
    assert_response :success
  end

  test '#show' do
    @user = users(:one)
    get api_user_path(@user), as: :json
    assert_response :success
  end
  # END
end
